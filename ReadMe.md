# Reverse Proxy

### with HTTP/2 & SSL support for local development

## Setup

1. Edit server.cnf to match the domain names that you would like to use. Remember to add Subject Alternate Names to make the cert serve on www.domain.com as well if you choose to.

   Source: https://somoit.net/security/security-create-self-signed-san-certificate-openssl

**Config file**

```bash
[ req ]
default_bits       = 4096
default_md         = sha512
default_keyfile    = server.key
prompt             = no
encrypt_key        = no
distinguished_name = req_distinguished_name
x509_extensions    = v3_req

# distinguished_name
[ req_distinguished_name ]
countryName            = "US"                                        # C=
stateOrProvinceName    = "California"                                # S=
localityName           = "San Francisco"                             # L=
organizationName       = "World of Q Technology Inc."                # O=
organizationalUnitName = "Rampage"                                   # OU=
commonName             = "example.com"                               # CN=
emailAddress           = "human@earth.org"                           # CN/emailAddress=

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names

[ alt_names ]
DNS.0 = "example.com"
DNS.1 = "www.example.com"
```

2. Generate SSL certificate and key using the command below.

```bash
openssl req -new -x509 -newkey rsa:2048 -sha256 -nodes -keyout server.key -days 3560 -out server.crt -config server.cnf
```

3. Start the proxy

```
yarn
yarn dev
```

4. Add entry in Hosts file, to access local development application using domain name.

```
127.0.0.1           example.com
```

## Notes

1. Manually get Let's Encrypt Certificate if you have access to your domain DNS settings (needed for validation to get SSL certs from Let's Encrypt)

- Follow the instructions as guided by https://zerossl.com

- Download the cert and key, use this in place of self-signed certificate generated in Setup : Step 2
