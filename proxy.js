const redbird = new require("redbird")({
  // Specify filenames to default SSL certificates (in case SNI is not supported by the
  // user's browser)
  ssl: {
    port: 443,
    key: "./server.key",
    cert: "./server.crt"
  }
});

redbird.register("example.com", "http://localhost:3000", {
  ssl: {
    key: "./server.key",
    cert: "./server.crt"
  }
});
